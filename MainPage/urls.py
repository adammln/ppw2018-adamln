from django.urls import path
from . import views

urlpatterns = [
    path('', views.loadBaseHTML, name='base'),
    path('', views.index, name='index'),

]
